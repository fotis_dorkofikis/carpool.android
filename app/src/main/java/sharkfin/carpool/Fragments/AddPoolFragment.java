package sharkfin.carpool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import okhttp3.internal.cache.InternalCache;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.CustomPointsController;
import sharkfin.carpool.ApiInterfaces.MyPoolController;
import sharkfin.carpool.ApiInterfaces.RoutesController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.CustomPoints.CustomPoint;
import sharkfin.carpool.Resources.CustomPoints.CustomPointResponse;
import sharkfin.carpool.Resources.MyPool.MyPools;
import sharkfin.carpool.Resources.Routes.RoutePoints;
import sharkfin.carpool.Resources.Routes.Routes;
import sharkfin.carpool.Resources.Routes.RoutesResponse;

public class AddPoolFragment extends Fragment {
    public ArrayList<String> tokenAndId = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_pool, container, false);
        EditText descriptionText = rootView.findViewById(R.id.poolDescription);
        EditText dateTimeText = rootView.findViewById(R.id.DatePickEditText);
        EditText freeSeatsText = rootView.findViewById(R.id.freeSeatsEditText);
        EditText vehiclePlatesText = rootView.findViewById(R.id.vehiclePlates);
        Button  saveAddPoolBtn = rootView.findViewById(R.id.saveAddPool);


        Bundle bundle = getArguments();

        tokenAndId = bundle.getStringArrayList("loginResponse");
        ArrayList<Integer> startEndPoint = new ArrayList<Integer>();
        startEndPoint = bundle.getIntegerArrayList("startEndPoint");

        MyPools myPool = new MyPools();

        RoutePoints routePointEnd = new RoutePoints();

        routePointEnd.setEnd(true);
        routePointEnd.setId(startEndPoint.get(1));

        RoutePoints routePointStart = new RoutePoints();

        routePointStart.setStart(true);
        routePointStart.setId(startEndPoint.get(0));

        Routes routes = new Routes();
        List<RoutePoints> routePoints = new ArrayList<RoutePoints>();

        routePoints.add(routePointStart);
        routePoints.add(routePointEnd);

        routes.setRoutePoints(routePoints);


        RoutesController controller = RoutesController.retrofit.create(RoutesController.class);

        Call<RoutesResponse> callForEnd = controller.createPost(routes);
        callForEnd.enqueue(new Callback<RoutesResponse>() {
            @Override
            public void onResponse(Call<RoutesResponse> call, Response<RoutesResponse> response) {
                if (!response.isSuccessful()) {

                    Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();

                } else {

                    myPool.setRouteId(response.body().getId());
                }

            }

            @Override
            public void onFailure(Call<RoutesResponse> call, Throwable t) {
                t.printStackTrace();
            }

        });

            saveAddPoolBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myPool.setDescription(descriptionText.getText().toString());
                String dateTimeString = dateTimeText.getText().toString();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                try {
                    Date date = format.parse(dateTimeString);
                    myPool.setStart(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                myPool.setFreeSeats(Integer.parseInt(freeSeatsText.getText().toString()));

                myPool.setUserId(Integer.parseInt(tokenAndId.get(0)));

                myPool.setVehicleId(vehiclePlatesText.getText().toString());

                MyPoolController controller = MyPoolController.retrofit.create(MyPoolController.class);

                Call<ResponseBody> callForEnd = controller.createPost(myPool);


                callForEnd.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (!response.isSuccessful()) {

                            Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();

                        } else {

                            Toast.makeText(getContext(), "Added Successfuly", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        t.printStackTrace();
                    }

                });



            }
        });






        return rootView;
    }
}
