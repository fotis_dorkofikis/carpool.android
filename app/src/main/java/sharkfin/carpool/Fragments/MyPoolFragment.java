package sharkfin.carpool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import sharkfin.carpool.R;

public class MyPoolFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_pool, container, false);
        Bundle bundle = getArguments();
        ArrayList<String> tokenAndId = new ArrayList<>();
        tokenAndId = bundle.getStringArrayList("loginResponse");


        return rootView;
    }


}
