package sharkfin.carpool.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.VehiclesController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.Vehicles.Vehicles;

public class VehiclesFragment extends Fragment {
    ArrayList<String> tokenAndId = new ArrayList<String>();
    ArrayList<String> listItems=new ArrayList<String>();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vehicles, container, false);
        Button addVehicle = rootView.findViewById(R.id.add);
        ListView carList = rootView.findViewById(R.id.carsListView);
        Button delete = rootView.findViewById(R.id.Delete);
        Bundle bundle = getArguments();

        tokenAndId = bundle.getStringArrayList("loginResponse");
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle b = new Bundle();

        addVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment vehiclesAddFragment = new VehiclesAddFragment();
                b.putStringArrayList("loginResponse",tokenAndId);
                vehiclesAddFragment.setArguments(b);
                fragmentTransaction.add(R.id.fragment_container, vehiclesAddFragment).commit();

            }
        });

        VehiclesController vehiclesController = VehiclesController.retrofit.create(VehiclesController.class);
        Call<List<Vehicles>> listCall = vehiclesController.GetVehicles();
        listCall.enqueue(new Callback<List<Vehicles>>() {
            @Override
            public void onResponse(Call<List<Vehicles>> call, Response<List<Vehicles>> response) {

                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Failed to cars", Toast.LENGTH_SHORT).show();
                } else {
                    List<Vehicles> vehiclesResponse = response.body();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listItems);
                    carList.setAdapter(adapter);
                    for (Vehicles n : vehiclesResponse) {
                        if(listItems.contains(n.getPlateNumber().toUpperCase())){

                        }
                        else
                        {
                        listItems.add(n.getPlateNumber().toUpperCase());
                        adapter.notifyDataSetChanged();
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<List<Vehicles>> call, Throwable t) {
                t.printStackTrace();
            }
        });
        return rootView;
    }





}
