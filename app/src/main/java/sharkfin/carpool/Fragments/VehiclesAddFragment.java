package sharkfin.carpool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.VehiclesController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.Vehicles.Vehicles;

public class VehiclesAddFragment extends Fragment {
    public   ArrayList<String> tokenAndId = new ArrayList<>();
    private VehiclesController vehiclesController;

    AwesomeValidation awesomeValidation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vehicles_add, container, false);

        Bundle bundle = getArguments();

        tokenAndId = bundle.getStringArrayList("loginResponse");


        Button save = rootView.findViewById(R.id.save);

        EditText plateText = rootView.findViewById(R.id.DatePickEditText);
        EditText numberOfSeats = rootView.findViewById(R.id.numberOfSeats);
        EditText model = rootView.findViewById(R.id.ModelText);
        EditText brand = rootView.findViewById(R.id.BrandEditText);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(plateText, RegexTemplate.NOT_EMPTY,"Please enter plates");
        awesomeValidation.addValidation(numberOfSeats, "[0-9]{1}","Please enter seats");
        awesomeValidation.addValidation(plateText, RegexTemplate.NOT_EMPTY,"Please enter plates");
        awesomeValidation.addValidation(plateText, RegexTemplate.NOT_EMPTY,"Please enter plates");

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (awesomeValidation.validate()){
                    CreatePost(plateText,numberOfSeats,model,brand);
                }
                else{
                    Toast.makeText(getContext(),"Validation Failed",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;

    }

    private void CreatePost(EditText plateText, EditText numberOfSeats, EditText modelText, EditText brandText){



       int seats = Integer.parseInt(numberOfSeats.getText().toString());
        String plates = plateText.getText().toString();
        String model = modelText.getText().toString();
        String brand = brandText.getText().toString();

        VehiclesController vehiclesController = VehiclesController.retrofit.create(VehiclesController.class);

        Vehicles vehicles = new Vehicles(plates,seats,model,brand);

        Call<ResponseBody> call = vehiclesController.createPost(vehicles);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(!response.isSuccessful()){
                    //todo pop up window for wrong credentials
                    Toast.makeText(getActivity(),"Failed",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getActivity()
                            ,"Your car saved successfully",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


}
