package sharkfin.carpool.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;

import sharkfin.carpool.R;
import sharkfin.carpool.Resources.Users.LoginResponse;

public class ProfileFragment extends Fragment {
    public ArrayList<String> tokenAndId=new ArrayList<String>();
    public Button editProfile;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        editProfile = rootView.findViewById(R.id.editButton);
        Bundle bundle = getArguments();

        tokenAndId = bundle.getStringArrayList("loginResponse");

        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle b = new Bundle();

        editProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment pr = new ProfileEditFragment();
                b.putStringArrayList("loginResponse",tokenAndId);
                pr.setArguments(b);
                fragmentTransaction.add(R.id.fragment_container, pr).commit();



                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, new ProfileEditFragment());
                fragmentTransaction.commit();
            }
        });

       // after authourization is complete

        return rootView;
    }


}
