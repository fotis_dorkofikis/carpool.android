package sharkfin.carpool.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.MyPoolController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.CustomPoints.CustomPoint;
import sharkfin.carpool.Resources.MyPool.MyPools;

import static sharkfin.carpool.R.id.poolListView;

public class JoinPoolFragment extends Fragment {
    ArrayList<String> listItems=new ArrayList<String>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_joinpool, container, false);
        ListView list = rootView.findViewById(poolListView);
        Bundle bundle = getArguments();
        ArrayList<String> tokenAndId = new ArrayList<>();
        tokenAndId = bundle.getStringArrayList("loginResponse");
        ArrayList<Integer> startEndPoint = new ArrayList<Integer>();
        startEndPoint = bundle.getIntegerArrayList("startEndPoint");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listItems);
        list.setAdapter(adapter);
        ArrayList<String> arrayList= new ArrayList<String>();
        arrayList.add("RMN5705");
        listItems.add(arrayList.get(0).toString());
        adapter.notifyDataSetChanged();

        CustomPoint start = new CustomPoint();
        start.setId(startEndPoint.get(0));

        CustomPoint end = new CustomPoint();
        end.setId(startEndPoint.get(1));

//        MyPoolController myPoolController = MyPoolController.retrofit.create(MyPoolController.class);
//
//        Call<MyPools> call = myPoolController.GetMyPoolsByStartEndPoints(start,end);
//
//
//        call.enqueue(new Callback<MyPools>() {
//            @Override
//            public void onResponse(Call<MyPools> call, Response<MyPools> response) {
//                if (!response.isSuccessful()) {
//
//                    Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();
//
//                } else {
//
//                    Toast.makeText(getContext(), "Added Successfuly", Toast.LENGTH_SHORT).show();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<MyPools> call, Throwable t) {
//                t.printStackTrace();
//            }
//
//        });
        return rootView;
    }
}
