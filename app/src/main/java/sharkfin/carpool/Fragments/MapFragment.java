package sharkfin.carpool.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.EncodedPolyline;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.CustomPointsController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.CustomPoints.CustomPoint;
import sharkfin.carpool.Resources.CustomPoints.CustomPointResponse;
import sharkfin.carpool.Resources.MyPool.PoolsCustomPointsId;
import sharkfin.carpool.Resources.Routes.Routes;

public class MapFragment extends Fragment {
    public PoolsCustomPointsId poolsCustomPointsId = new PoolsCustomPointsId();
    public Marker possitionMarker;
    public GeoApiContext geoApiContext = new GeoApiContext.Builder().apiKey("AIzaSyBH-ULR96P4fnZ8rgK7M-e7s_w4IR-yKSI").build(); //("AIzaSyBH-ULR96P4fnZ8rgK7M-e7s_w4IR-yKSI"));
    public LatLng placeLatLng;
    public CustomPoint customPointStart = new CustomPoint();
    public CustomPoint customPointEnd = new CustomPoint();
    ArrayList<String> tokenAndId = new ArrayList<>();
    CardView cardView;
    Button findPool;
    Button addPool;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!Places.isInitialized()) {
            Places.initialize(getContext(), "AIzaSyBH-ULR96P4fnZ8rgK7M-e7s_w4IR-yKSI");
        }
        ;

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment); //.findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Toast.makeText(getContext(), place.getName(), Toast.LENGTH_SHORT).show();
                //Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
            }

            @Override
            public void onError(@NonNull Status status) {
                Toast.makeText(getContext(), status.getStatusCode(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        CardView cardView = (CardView) rootView.findViewById(R.id.cv);
        cardView.setVisibility(View.GONE);
        addPool = rootView.findViewById(R.id.addPool);
        findPool = rootView.findViewById(R.id.joinPool);

        Bundle bundle = getArguments();
        tokenAndId = bundle.getStringArrayList("loginResponse");


        //Create Map Fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {

                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mMap.clear(); //clear old markers

                CustomPointsController customPointsController = CustomPointsController.retrofit.create(CustomPointsController.class);
                Call<CustomPointResponse> call = customPointsController.GetCustomPointById(1);

                //Get user location after permission check
                LatLng userLatLng = CheckPermissionAndGetDeviceLocation();

                if (!Places.isInitialized()) {
                    Places.initialize(getContext(), "AIzaSyBH-ULR96P4fnZ8rgK7M-e7s_w4IR-yKSI");
                }
                ;


                AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment) getChildFragmentManager().findFragmentById(R.id.autocomplete_fragment); //.findFragmentById(R.id.autocomplete_fragment);

                autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME));

                autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                    @Override
                    public void onPlaceSelected(Place place) {
                        // TODO: Get info about the selected place.

                        if (Geocoder.isPresent()) {
                            try {
                                String location = place.getName();
                                Geocoder gc = new Geocoder(getContext());
                                List<Address> addresses = gc.getFromLocationName(location, 5); // get the found Address Objects

                                List<LatLng> ll = new ArrayList<LatLng>(addresses.size()); // A list to save the coordinates if they are available
                                for (Address a : addresses) {
                                    if (a.hasLatitude() && a.hasLongitude()) {
                                        placeLatLng = new LatLng(a.getLatitude(), a.getLongitude());
                                    }
                                }
                            } catch (IOException e) {
                                e.getStackTrace();
                            }
                        }
                        // Toast.makeText(getContext(), place.g .toString(), Toast.LENGTH_SHORT).show();
                        LatLng newLatlng = new LatLng((placeLatLng.latitude + userLatLng.latitude) / 2, (placeLatLng.longitude + userLatLng.longitude) / 2);
                        CameraPosition googlePlex = CameraPosition.builder().target(newLatlng).zoom(40).bearing(40).tilt(45).build();
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 40, null);

                        CustomPointsController customPointsController = CustomPointsController.retrofit.create(CustomPointsController.class);
                        CustomPoint customPointStart = new CustomPoint(userLatLng.latitude, userLatLng.longitude, "Your destination", true);
                        Call<CustomPointResponse> callForStart = customPointsController.createPost(customPointStart);
                        callForStart.enqueue(new Callback<CustomPointResponse>() {
                            @Override
                            public void onResponse(Call<CustomPointResponse> call, Response<CustomPointResponse> response) {
                                if (!response.isSuccessful()) {

                                    Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();

                                } else {
                                    poolsCustomPointsId.setCustomPointStart(response.body().getId());
                                }

                            }

                            @Override
                            public void onFailure(Call<CustomPointResponse> call, Throwable t) {
                                t.printStackTrace();
                            }

                        });


                        CustomPointsController customPointsController2 = CustomPointsController.retrofit.create(CustomPointsController.class);
                        CustomPoint customPointEnd = new CustomPoint(placeLatLng.latitude, placeLatLng.longitude, "Your destination", true);
                        Call<CustomPointResponse> callForEnd = customPointsController2.createPost(customPointEnd);
                        callForEnd.enqueue(new Callback<CustomPointResponse>() {
                            @Override
                            public void onResponse(Call<CustomPointResponse> call, Response<CustomPointResponse> response) {
                                if (!response.isSuccessful()) {

                                    Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();

                                } else {
                                    poolsCustomPointsId.setCustomPointEnd(response.body().getId());
                                }

                            }

                            @Override
                            public void onFailure(Call<CustomPointResponse> call, Throwable t) {
                                t.printStackTrace();
                            }

                        });

                        possitionMarker = mMap.addMarker(new MarkerOptions().position(placeLatLng));

                        DrawRoute(userLatLng, placeLatLng, mMap);
                        cardView.setVisibility(View.VISIBLE);

                    }

                    @Override
                    public void onError(@NonNull Status status) {
                        Toast.makeText(getContext(), status.getStatusCode(), Toast.LENGTH_SHORT).show();
                    }
                });


                CameraPosition googlePlex = CameraPosition.builder().target(userLatLng).zoom(15).bearing(0).tilt(45).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 7, null);
                mMap.setMyLocationEnabled(true);
            }
        });
        findPool.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Bundle b = new Bundle();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment joinPoolFragment = new JoinPoolFragment();
                ArrayList<Integer> startEndPoint = new ArrayList<Integer>();
                startEndPoint.add(poolsCustomPointsId.getCustomPointStart());
                startEndPoint.add(poolsCustomPointsId.getCustomPointEnd());
                b.putIntegerArrayList("startEndPoint",startEndPoint);
                b.putStringArrayList("loginResponse", tokenAndId);
                joinPoolFragment.setArguments(b);
                fragmentTransaction.add(R.id.fragment_container, joinPoolFragment).commit();
            }
        });
        addPool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                Bundle b = new Bundle();
                ArrayList<Integer> startEndPoint = new ArrayList<Integer>();
                startEndPoint.add(poolsCustomPointsId.getCustomPointStart());
                startEndPoint.add(poolsCustomPointsId.getCustomPointEnd());
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment addPoolFragment = new AddPoolFragment();
                b.putStringArrayList("loginResponse", tokenAndId);
                b.putIntegerArrayList("startEndPoint",startEndPoint);
                addPoolFragment.setArguments(b);
                fragmentTransaction.add(R.id.fragment_container, addPoolFragment).commit();
            }
        });


        return rootView;
    }


//------------------------------------------------------------METHODS------------------------------------------------METHODS----------------------------------------------------METHODS------------------------------------------------------------

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private LatLng CheckPermissionAndGetDeviceLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        LatLng userLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        return userLatLng;
    }


//    private int  InsertMarker(LatLng latLng, GoogleMap mMap) {
//
//        final int[] id = {0};
//        CustomPointsController customPointsController = CustomPointsController.retrofit.create(CustomPointsController.class);
//        CustomPoint customPoint = new CustomPoint(latLng.latitude, latLng.longitude, "Your destination", true);
//        Call<CustomPointResponse> call = customPointsController.createPost(customPoint);
//        call.enqueue(new Callback<CustomPointResponse>() {
//            @Override
//            public void onResponse(Call<CustomPointResponse> call, Response<CustomPointResponse> response) {
//                if (!response.isSuccessful()) {
//
//                    Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();
//
//                } else {
//                    id[0] =response.body().getId();
//                }
//
//            }
//            @Override
//            public void onFailure(Call<CustomPointResponse> call, Throwable t) {
//                t.printStackTrace();
//            }
//
//        });
//        return id[0];
//    }


    private void DrawRoute(LatLng userLatLng, LatLng latLng, GoogleMap mMap) {
        List<LatLng> path = new ArrayList();
        DirectionsApiRequest req = DirectionsApi.getDirections(geoApiContext, userLatLng.latitude + "," + userLatLng.longitude, latLng.latitude + "," + latLng.longitude);
        try {
            DirectionsResult res = req.await();
            //Loop through legs and steps to get encoded polylines of each step
            if (res.routes != null && res.routes.length > 0) {
                DirectionsRoute route = res.routes[0];

                if (route.legs != null) {
                    for (int i = 0; i < route.legs.length; i++) {
                        DirectionsLeg leg = route.legs[i];
                        if (leg.steps != null) {
                            for (int j = 0; j < leg.steps.length; j++) {
                                DirectionsStep step = leg.steps[j];
                                if (step.steps != null && step.steps.length > 0) {
                                    for (int k = 0; k < step.steps.length; k++) {
                                        DirectionsStep step1 = step.steps[k];
                                        EncodedPolyline points1 = step1.polyline;
                                        if (points1 != null) {
                                            //Decode polyline and add points to list of route coordinates
                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
                                            for (com.google.maps.model.LatLng coord1 : coords1) {
                                                path.add(new LatLng(coord1.lat, coord1.lng));
                                            }
                                        }
                                    }
                                } else {
                                    EncodedPolyline points = step.polyline;
                                    if (points != null) {
                                        //Decode polyline and add points to list of route coordinates
                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
                                        for (com.google.maps.model.LatLng coord : coords) {
                                            path.add(new LatLng(coord.lat, coord.lng));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
        //Draw the polyline
        if (path.size() > 0) {
            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLUE).width(5);
            mMap.addPolyline(opts);
        }
    }
}


//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//                  OLD NOTES

//                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//
//                    @Override
//                    public void onMapClick(LatLng latLng) {
//
//                        CustomPointsController customPointsController = CustomPointsController.retrofit.create(CustomPointsController.class);
//                        CustomPoint customPoint = new CustomPoint(latLng.latitude, latLng.longitude, "Your destination", true);
//                        Call<ResponseBody> call = customPointsController.createPost(customPoint);
//                        call.enqueue(new Callback<ResponseBody>() {
//                            @Override
//                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                                if (!response.isSuccessful()) {
//                                    Toast.makeText(getContext(), "A problem occurred", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    mMap.addMarker(new MarkerOptions().position(latLng).title("Your destination").snippet("Latitude: " + latLng.latitude + "\nLongitude : " + latLng.longitude));
//                                    Toast.makeText(getContext(), "Destination Added Successfully", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//
//                            @Override
//                            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                                t.printStackTrace();
//                            }
//                        });
//
//
//                    }
//                });

//Long Click Test                mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
//                    @Override
//                    public void onMapLongClick(LatLng latLng) {
//                        CustomPointsController customPointsController1 = CustomPointsController.retrofit.create(CustomPointsController.class);
//                        Call<List<CustomPointResponse>> listCall = customPointsController1.GetCustomPoints();
//                        listCall.enqueue(new Callback<List<CustomPointResponse>>() {
//                            @Override
//                            public void onResponse(Call<List<CustomPointResponse>> call, Response<List<CustomPointResponse>> response) {
//
//                                if (!response.isSuccessful()) {
//                                    Toast.makeText(getContext(), "Failed to load home Location", Toast.LENGTH_SHORT).show();
//                                }
//                                else {
//                                    List<CustomPointResponse> customPointResponses = response.body();
//                                    for (CustomPointResponse customPoint: customPointResponses) {
//
//                                        LatLng tempPointLatLng = new LatLng(customPoint.getLatitude(), customPoint.getLongitude());
//                                        mMap.addMarker(new MarkerOptions().position(tempPointLatLng).title(customPoint.getDescription()).
//                                                snippet("Latitude: " + tempPointLatLng.latitude + " \n Longitude: " + tempPointLatLng.longitude));
//                                        Toast.makeText(getContext(), "Successfully loaded Location", Toast.LENGTH_SHORT).show();
//
//                                    }
//
//                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(Call<List<CustomPointResponse>> call, Throwable t) {
//                                t.printStackTrace();
//                            }
//                        });
//                    }
//                });
