package sharkfin.carpool.Fragments;

import android.os.Bundle;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import java.util.ArrayList;

import sharkfin.carpool.R;

public class ProfileEditFragment extends Fragment {
    Button save;
    EditText emailEditText;
    EditText phoneEditText;
    AwesomeValidation awesomeValidation;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        View rootView = inflater.inflate(R.layout.fragment_profile_edit, container, false);
        phoneEditText = rootView.findViewById(R.id.phoneText);
        emailEditText = rootView.findViewById(R.id.emailText);

        awesomeValidation.addValidation(emailEditText, Patterns.EMAIL_ADDRESS,"Please enter a valid email");
        awesomeValidation.addValidation(phoneEditText,"[6]{1}[9]{1}[0-9]{8}$","Please enter a valid phone number");

        Bundle bundle = getArguments();
        ArrayList<String> tokenAndId = new ArrayList<>();
        tokenAndId = bundle.getStringArrayList("loginResponse");


        save = rootView.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CreatePost();
                if(awesomeValidation.validate()){
//
                    CreatePost(phoneEditText,emailEditText);
                }
                else{
                    Toast.makeText(getContext(),"Validation Failed",Toast.LENGTH_SHORT).show();
                }

            }
        });
        return rootView;
    }

    public void CreatePost(EditText phone , EditText email){

        long phoneNumber = !phone.getText().toString().equals("")?Long.parseLong(phone.getText().toString()) : 0;
        String emailAddress = email.getText().toString();

        // in the end after authorizattion tested

    }
}


