package sharkfin.carpool.ApiInterfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import sharkfin.carpool.Resources.Users.Register;

public interface RegisterController {

    @POST("api/Users/Register")
    Call<ResponseBody> createPost(@Body Register register );

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
