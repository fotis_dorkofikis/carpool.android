package sharkfin.carpool.ApiInterfaces;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.CustomPoints.CustomPoint;
import sharkfin.carpool.Resources.CustomPoints.CustomPointResponse;

public interface CustomPointsController {
    @GET("api/CustomPoints")
    Call<List<CustomPointResponse>> GetCustomPoints();

    @GET("api/CustomPoints/{id}")
    Call<CustomPointResponse> GetCustomPointById(@Path("id") int id);

    @POST("api/CustomPoints")
    Call<CustomPointResponse> createPost(@Body CustomPoint customPoint);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
