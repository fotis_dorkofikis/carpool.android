package sharkfin.carpool.ApiInterfaces;

import com.google.gson.JsonObject;
import com.google.maps.model.Vehicle;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.Vehicles.Vehicles;

public interface VehiclesController {

    @GET("api/Vehicles")
    Call<List<Vehicles>> GetVehicles();

    @GET("api/Vehicles/{id}")
    Call<Vehicles> GetVehiclesById(@Path("id") int id);

    @POST("api/Vehicles")
    Call<ResponseBody> createPost(@Body Vehicles vehicles);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
