package sharkfin.carpool.ApiInterfaces;

import java.util.List;

import okhttp3.ResponseBody;
import okhttp3.Route;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.Routes.Routes;
import sharkfin.carpool.Resources.Routes.RoutesResponse;

public interface RoutesController {

    @GET("api/Routes")
    Call<List<Routes>> GetRoutes();

    @GET("api/Routes/{id}")
    Call<Routes> GetRoutesById(@Path("id") int id);

    @POST("api/Routes")
    Call<RoutesResponse> createPost(@Body Routes route);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
