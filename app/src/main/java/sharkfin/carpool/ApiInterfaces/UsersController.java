package sharkfin.carpool.ApiInterfaces;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.Users.UserResource;

public interface UsersController {
    @GET("api/Users")
    Call<List<UserResource>> GetUsers();

    @GET("api/Users/{id}")
    Call<UserResource> GetUserById(@Path("id") int id);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
