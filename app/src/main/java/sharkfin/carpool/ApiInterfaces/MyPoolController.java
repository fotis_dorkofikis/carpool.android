package sharkfin.carpool.ApiInterfaces;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.CustomPoints.CustomPoint;
import sharkfin.carpool.Resources.MyPool.MyPools;

public interface MyPoolController {
    @GET("api/MyPool")
    Call<List<MyPools>> GetMyPool();

    @GET("api/MyPool/{id}")
    Call<MyPools> GetMyPoolById(@Path("id") int id);

    @GET("api/GetPoolsByStartEndPoints")
    Call<MyPools> GetMyPoolsByStartEndPoints(CustomPoint start, CustomPoint end);


    @POST("api/MyPool")
    Call<ResponseBody> createPost(@Body MyPools myPool);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
