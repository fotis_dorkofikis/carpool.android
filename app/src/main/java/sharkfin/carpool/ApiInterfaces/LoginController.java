package sharkfin.carpool.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import sharkfin.carpool.Resources.Users.Login;
import sharkfin.carpool.Resources.Users.LoginResponse;

public interface LoginController {

   //@FormUrlEncoded
    @POST("/api/Users/Login")
    Call<LoginResponse> createPost(@Body() Login login);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();


}
