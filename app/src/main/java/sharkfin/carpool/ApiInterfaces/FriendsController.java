package sharkfin.carpool.ApiInterfaces;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.Friends.Friends;

public interface FriendsController {

    @GET("api/Friends")
    Call<List<Friends>> GetFriends();

    @GET("api/Friends/{id}")
    Call<Friends> GetFriendById(@Path("id") int id);

    @POST("api/Friends")
    Call<ResponseBody> createPost(@Body Friends friend);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
