package sharkfin.carpool.ApiInterfaces;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import sharkfin.carpool.Resources.FavoriteRoutes.FavoriteRoutes;

public interface FavoriteRoutesController {

    @GET("api/FavoriteRoutes")
    Call<List<FavoriteRoutes>> GetFavoriteRoutes();

    @GET("api/FavoriteRoutes/{id}")
    Call<FavoriteRoutes> GetFavoritesRoutesById(@Path("id") int id);

    @POST("api/FavoriteRoutes")
    Call<ResponseBody> createPost(@Body FavoriteRoutes favoriteRoutes);

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://10.0.2.2:5000/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
