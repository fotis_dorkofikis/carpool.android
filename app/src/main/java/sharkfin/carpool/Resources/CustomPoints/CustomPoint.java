package sharkfin.carpool.Resources.CustomPoints;

public class CustomPoint {
    private int Id;
    private double Latitude;
    private double Longitude;
    private String Description;
    private boolean isPublic;

    public CustomPoint(double latitude, double longitude, String description, boolean isPublic) {
        Latitude = latitude;
        Longitude = longitude;
        Description = description;
        this.isPublic = isPublic;
    }

    public CustomPoint(){

    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}
