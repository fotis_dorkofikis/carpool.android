package sharkfin.carpool.Resources.Users;


public class Register {
    String password;
    String username;
    UserDataResource userDataResource = new UserDataResource();
    UserResource userResource = new UserResource();

    public UserDataResource getUserDataResource() {
        return userDataResource;
    }

    public void setUserDataResource(UserDataResource userDataResource) {
        this.userDataResource = userDataResource;
    }

//    public UserResource getUserResource() {
//        return userResource;
//    }
//
//    public void setUserResource(UserResource userResource) {
//        this.userResource = userResource;
//    }


    public Register(String userName, String pass, String firstName, String lastName, String email, long phone) {
        userDataResource.setFirstName(firstName);
        userDataResource.setLastName(lastName);
        userDataResource.setEmail(email);
        userDataResource.setPhone(phone);
        username = userName;
        password = pass;
//        userResource.setUsername(userName);
//        userResource.setPassword(password);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
