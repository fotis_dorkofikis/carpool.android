package sharkfin.carpool.Resources.Users;

import android.os.Parcelable;

import java.io.Serializable;

public class LoginResponse implements Serializable {
    String token;
    int userId;

    public String getToken() {
        return token;
    }

    public int getUserId() {
        return userId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
