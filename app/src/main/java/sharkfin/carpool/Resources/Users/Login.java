package sharkfin.carpool.Resources.Users;

public class Login {
    public String username;
    public String password;
    public String token;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getToken() {
        return token;
    }

    public Login(String username, String password, String token) {
        this.username = username;
        this.password = password;
        this.token = token;
    }

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Login() {

    }
}
