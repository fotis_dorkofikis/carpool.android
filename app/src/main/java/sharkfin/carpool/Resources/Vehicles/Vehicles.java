package sharkfin.carpool.Resources.Vehicles;

public class Vehicles {
String plateNumber;
int numberOfSeats;
String Model;
String Brand;
int userId;

    public Vehicles(String plateNumber, int numberOfSeats, String model, String brand) {
        this.plateNumber = plateNumber;
        this.numberOfSeats = numberOfSeats;
        Model = model;
        Brand = brand;
        userId=1;
    }
    public Vehicles(){};

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
