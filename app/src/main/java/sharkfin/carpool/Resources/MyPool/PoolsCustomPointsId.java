package sharkfin.carpool.Resources.MyPool;

import sharkfin.carpool.Resources.CustomPoints.CustomPoint;

public class PoolsCustomPointsId {

    int customPointStart ;
    int customPointEnd ;

    public int getCustomPointStart() {
        return customPointStart;
    }

    public void setCustomPointStart(int customPointStart) {
        this.customPointStart = customPointStart;
    }

    public int getCustomPointEnd() {
        return customPointEnd;
    }

    public void setCustomPointEnd(int customPointEnd) {
        this.customPointEnd = customPointEnd;
    }
}
