package sharkfin.carpool.Resources.Routes;

import com.google.android.gms.maps.model.LatLng;

public class RoutePoints {
    int Id;
    String Description;
    LatLng Latitude;
    LatLng Longitude;
    boolean IsStart;
    boolean IsEnd;
    boolean IsPublic;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public LatLng getLatitude() {
        return Latitude;
    }

    public void setLatitude(LatLng latitude) {
        this.Latitude = latitude;
    }

    public LatLng getLongitude() {
        return Longitude;
    }

    public void setLongitude(LatLng longitude) {
        this.Longitude = longitude;
    }

    public boolean isStart() {
        return IsStart;
    }

    public void setStart(boolean start) {
        IsStart = start;
    }

    public boolean isEnd() {
        return IsEnd;
    }

    public void setEnd(boolean end) {
        IsEnd = end;
    }

    public boolean isPublic() {
        return IsPublic;
    }

    public void setPublic(boolean aPublic) {
        IsPublic = aPublic;
    }
}
