package sharkfin.carpool.Resources.Routes;

import java.util.ArrayList;
import java.util.List;

public class Routes {
    int id;
    String description;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

   List<RoutePoints> routePoints = new ArrayList<>();

    public void setRoutePoints(List<RoutePoints> routePoints) {
        this.routePoints = routePoints;
    }

    public List<RoutePoints> getRoutePoints() {
        return routePoints;
    }
}
