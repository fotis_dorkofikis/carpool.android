package sharkfin.carpool.Resources.Routes;

import java.util.ArrayList;
import java.util.List;

public class RoutesResponse {

    int Id;

    String Description;
    List<RoutePoints> routePoints = new ArrayList<RoutePoints>();

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public List<RoutePoints> getRoutePoints() {
        return routePoints;
    }

    public void setRoutePoints(List<RoutePoints> routePoints) {
        this.routePoints = routePoints;
    }
}
