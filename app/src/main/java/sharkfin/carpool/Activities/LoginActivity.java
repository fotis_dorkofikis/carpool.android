package sharkfin.carpool.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.LoginController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.Users.Login;
import sharkfin.carpool.Resources.Users.LoginResponse;

public class LoginActivity extends AppCompatActivity {
    public Button loginButton;
    public TextView registerButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);

        registerButton = findViewById(R.id.signupTextView);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRegisterActivity();
            }
        });
        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreatePost();
            }
        });
    }


    public void openRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }


    public void CreatePost() {
        final TextView popup = findViewById(R.id.errorTextView);
        EditText usernametxt = findViewById(R.id.userEditText);
        String username = usernametxt.getText().toString();
        EditText passwordtxt = findViewById(R.id.loginPasswordEditText);
        String password = passwordtxt.getText().toString();


        LoginController loginController = LoginController.retrofit.create(LoginController.class);

        Login login = new Login(username, password);


        Call<LoginResponse> call = loginController.createPost(login);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (!response.isSuccessful()) {
                    try {
                        Toast.makeText(getApplicationContext(), response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        e.printStackTrace();
                    }
                } else {

                    LoginResponse loginResponse = response.body();
                    int userId = response.body().getUserId();
                    String token = response.body().getToken();


                    openMainActivity(loginResponse);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
    public void openMainActivity(LoginResponse loginResponse) {
        ArrayList<String> temp = new ArrayList<>();
        temp.add(String.valueOf(loginResponse.getUserId()));
        temp.add(loginResponse.getToken());
        Intent intent = new Intent(this,MainActivity.class);
        intent.putExtra("response",temp);
        startActivity(intent);
    }
}
