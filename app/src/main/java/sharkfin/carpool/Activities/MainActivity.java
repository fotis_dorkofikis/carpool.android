package sharkfin.carpool.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener;


import java.util.ArrayList;

import sharkfin.carpool.Fragments.FavoriteRoutesFragment;
import sharkfin.carpool.Fragments.FriendsFragment;
import sharkfin.carpool.Fragments.MapFragment;
import sharkfin.carpool.Fragments.MyPoolFragment;
import sharkfin.carpool.Fragments.ProfileFragment;
import sharkfin.carpool.Fragments.VehiclesAddFragment;
import sharkfin.carpool.Fragments.VehiclesFragment;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.Users.LoginResponse;


public class MainActivity extends AppCompatActivity implements OnNavigationItemSelectedListener {
        private DrawerLayout drawer;
    public LoginResponse loginResponse = new LoginResponse();
    public ArrayList<String> tokenAndId = new ArrayList<>();


    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            Context mContext;

            Bundle bundle = getIntent().getExtras();

            ArrayList<String> temp =  bundle.getStringArrayList("response");
            String tempID =String.valueOf(temp.get(0));
            String tempToken = temp.get(1);

            tokenAndId.add(tempID);
            tokenAndId.add(tempToken);

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            drawer = findViewById(R.id.drawer_layout);

            NavigationView navigationView = findViewById(R.id.nav_view);

            navigationView.setNavigationItemSelectedListener(this);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                    R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();

            if (savedInstanceState == null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Fragment mapFrag = new MapFragment();
                Bundle b = new Bundle();
                b.putStringArrayList("loginResponse",tokenAndId);
                mapFrag.setArguments(b);
                fragmentTransaction.add(R.id.fragment_container, mapFrag).commit();
            }
        }




    @Override
        public boolean onNavigationItemSelected (@NonNull MenuItem item){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle b = new Bundle();
            switch (item.getItemId()) {

                case R.id.nav_friends:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new FriendsFragment()).commit();

                    break;

                case R.id.nav_favorite:
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                            new FavoriteRoutesFragment()).commit();
                    break;

                case R.id.nav_my_pool:
                    Fragment poolFrag = new MyPoolFragment();
                    b.putStringArrayList("loginResponse",tokenAndId);
                    poolFrag.setArguments(b);
                    fragmentTransaction.add(R.id.fragment_container, poolFrag).commit();
                    break;

                case R.id.nav_profile:

                    Fragment profileFrag = new ProfileFragment();
                    b.putStringArrayList("loginResponse",tokenAndId);
                    profileFrag.setArguments(b);
                    fragmentTransaction.add(R.id.fragment_container, profileFrag).commit();
                    break;

                case R.id.nav_vehicles:
                    Fragment vehiclesFrag = new VehiclesFragment();
                    b.putStringArrayList("loginResponse",tokenAndId);
                    vehiclesFrag.setArguments(b);
                    fragmentTransaction.add(R.id.fragment_container, vehiclesFrag).commit();
                    break;

                case R.id.nav_home:
                    Fragment mapFragm = new MapFragment();
                    b.putStringArrayList("loginResponse",tokenAndId);
                    mapFragm.setArguments(b);
                    fragmentTransaction.add(R.id.fragment_container, mapFragm).commit();
                    break;
            }
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

        @Override
        public void onBackPressed () {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
        }



}