package sharkfin.carpool.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sharkfin.carpool.ApiInterfaces.RegisterController;
import sharkfin.carpool.R;
import sharkfin.carpool.Resources.Users.Register;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;


public class RegisterActivity extends AppCompatActivity {

    private RegisterController registerController;

    AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Button button = findViewById(R.id.signupButton);

        EditText usernametxt = findViewById(R.id.nicknameEditText);
        EditText passwordtxt = findViewById(R.id.loginPasswordEditText);
        EditText firstnametxt = findViewById(R.id.firstnameEditText);
        EditText lastnametxt = findViewById(R.id.lastnameEditText);
        EditText emailtxt = findViewById(R.id.emailText);
        EditText phonetxt = findViewById(R.id.phoneText);


        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(this,R.id.nicknameEditText, RegexTemplate.NOT_EMPTY,R.string.invalidUsername);

        awesomeValidation.addValidation(this,R.id.firstnameEditText, RegexTemplate.NOT_EMPTY,R.string.invalidFirstname);

        awesomeValidation.addValidation(this,R.id.lastnameEditText, RegexTemplate.NOT_EMPTY,R.string.invalidLastname);

        awesomeValidation.addValidation(this, R.id.loginPasswordEditText,".{6,}",R.string.invalidPass);

        awesomeValidation.addValidation(this, R.id.rePasswordEditText,R.id.loginPasswordEditText,R.string.invalidRepassword);

        awesomeValidation.addValidation(this,R.id.phoneText,"[6]{1}[9]{1}[0-9]{8}$",R.string.invalidPhone);

        awesomeValidation.addValidation(this,R.id.emailText, Patterns.EMAIL_ADDRESS,R.string.invalidEmail);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    //CreatePost();
                if(awesomeValidation.validate()){
//
                    CreatePost();
                }
                else{
                    Toast.makeText(getApplicationContext(),"Validation Failed",Toast.LENGTH_SHORT).show();
                }
            }
        });




    }




    private void CreatePost(){

        EditText usernametxt = findViewById(R.id.nicknameEditText);

        String username =usernametxt.getText().toString();
        EditText passwordtxt = findViewById(R.id.loginPasswordEditText);

        String password =passwordtxt.getText().toString();
        EditText firstnametxt = findViewById(R.id.firstnameEditText);

        String firstname =firstnametxt.getText().toString();
        EditText lastnametxt = findViewById(R.id.lastnameEditText);

        String lastname =lastnametxt.getText().toString();
        EditText emailtxt = findViewById(R.id.emailText);

        String email =emailtxt.getText().toString();
        EditText phonetxt = findViewById(R.id.phoneText);
        long phone = !phonetxt.getText().toString().equals("")?Long.parseLong(phonetxt.getText().toString()) : 0;


        RegisterController registerController = RegisterController.retrofit.create(RegisterController.class);

        Register register = new Register(username,password,firstname,lastname,email,phone);

        Call<ResponseBody> call = registerController.createPost(register);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(!response.isSuccessful()){
                    //todo pop up window for wrong credentials
                    Toast.makeText(getApplicationContext(),"Validation Failed",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext()
                            ,"You registered successfully",Toast.LENGTH_SHORT).show();
                    openLoginActivity();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }



    public void openLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
